// miniprogram/pages/gdevice/gdevice.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    devices: [{
        "Name": "空调",
        b:true,
        "Type": "0401",
        "Count": 0
      }, {
        "Count": 0,
        "Name": "地暖",
         b:true,
        "Type": "0403",
      }, {
        "Count": 0,
        "Name": "DvD",
        "Type": "0409",
        "Count": 0
      }, {
        "Name": "功放",
        "Type": "0406",
        "Count": 0
      }, {
        "Name": "投影仪",
        "Type": "0408",
        "Count": 0
      }, {
        "Name": "背景音乐",
        "Type": "0405",
        "Count": 0
      },
      {
        "Name": "电视",
        "Type": "0402",
        "Count": 0
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
  },
 
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.get();
    wx.stopPullDownRefresh({
      complete: (res) => {},
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  deviceClick: function (e) {
    var index = e.currentTarget.dataset.index;
    var type = this.data.devices[index].Type;

    wx.navigateTo({
      url: './glist/glist?type=' + type,
    })
  }
})