// pages/scene/edit/edit.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    clock: {
      id: 0,
      displayName:'闹钟',
      week0: true,
      week1: true,
      week2: true,
      week3: true,
      week4: true,
      week5: true,
      week6: true,
      loop: true,
      enable: true,
      hour: "00",
      minute: "00",
      second:'00',
      actionCount:2,
    },
    timeValue:'08:00'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  checked: function () {
    var clock = this.data.clock;
    clock.enable = clock.enable ? false : true
    console.log(clock)
    this.setData({
      clock: clock
    })
  },
  action: function () {
    var clock = this.data.clock;
    wx.navigateTo({
      url: '../action/action?id=' + clock.id,
    })
  },
  InputChange: function (e) {
    var clock = this.data.clock;
    clock.displayName = e.detail.value
    console.log(clock)
    this.setData({
      clock: clock
    })
  },
  timePickerBindchange: function (e) {
    var clock = this.data.clock;
    var str = e.detail.value.split(':');
    clock.hour = str[0]
    clock.minute = str[1]
    this.setData({
      timeValue: e.detail.value,
      clock: clock
    })
  },
  zhouqi: function (e) {
    var clock = this.data.clock;
    wx.navigateTo({
      url: '../week/week?clock=' + JSON.stringify(clock),
    })
  },
  submitForm: function (e) {
    wx.navigateBack({
      complete: (res) => {},
    })
  }
})