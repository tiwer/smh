// miniprogram/pages/scene/week/week.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    clock: {
      week0: false,
      week1: false,
      week2: false,
      week3: false,
      week4: false,
      week5: false,
      week6: false,
      loop: false,
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var clock = JSON.parse(options.clock);
    if (clock != null) {
      console.log(clock.week0);
      this.setData({
        clock: clock
      })
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  choose: function (e) {
    var index = e.target.dataset.index;
    var clock = this.data.clock;

    switch (index) {
      case '0':
        console.log(clock)
        clock.week0 = clock.week0  ? false : true
        break;
      case '1':
        clock.week1 = clock.week1  ? false : true
        break;
      case '2':
        clock.week2 = clock.week2 ? false : true
        break;
      case '3':
        clock.week3 = clock.week3 ? false : true
        break;
      case '4':
        clock.week4 = clock.week4 ? false : true
        break;
      case '5':
        clock.week5 = clock.week5 ? false : true
        break;
      case '6':
        clock.week6 = clock.week6  ? false : true
        break;
      case '7':
        clock.loop = clock.loop ? false : true
        break;
    }

    this.setData({
      clock: clock
    })

  },
  submitForm: function () {
    var pages = getCurrentPages(); //当前页面
    var prevPage = pages[pages.length - 2]; //上一页面

    var clock = this.data.clock;
    prevPage.setData({
      clock: clock
    })
    wx.navigateBack()
  }
})