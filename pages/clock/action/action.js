// miniprogram/pages/scene/ation/ation.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    kg: ["打开", "关闭"],
    clockId: 0,
    list: [],
    dic: {
      "0101": {
        code: "14",
        start: 4,
        action: {
          "打开": "01",
          "关闭": "00"
        }
      },
      "0102": {
        code: "14",
        start: 4,
        action: {
          "打开": "01",
          "关闭": "00",
          "最亮": '64',
          "最暗": '0A'
        }
      },
      "0201": {
        code: "14",
        start: 4,
        action: {
          "打开": "01",
          "关闭": "00"
        }
      },
      "0202": {
        code: "14",
        start: 4,
        action: {
          "打开": "02",
          "停止": "01",
          "关闭": "00"
        }
      },
      "00FC": {
        code: "19",
        start: 3,
        action: {
          "启动": "FF",
        }
      },
      "0501": {
        code: "14",
        start: 0,
        action: {
          "开机": "01",
          "关机": "00",
          "制冷": "02",
          "制热": "03",
          "抽湿": "04",
          "制冷": "05",
          "自动": "06",
        }
      }
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '动作设置',
    })

    this.list();
  },

  list: function () {
    var list = [{
      name: "厨房开关",
      type: '0101',
      info: '打开'
    }, {
      name: "卧室空调",
      type: '0401',
      info: '制热'
    }]
    var that=this;
    
    for (let index = 0; index < list; index++) {
      var element = list[index];
      var ac = that.data.dic[element.type]
      element.checked = []
      if (!ac) {
        ac = that.data.dic["0101"]
      }
      for (var key in ac.action) {
        element.checked.push(key)
      }
    }
    this.setData({
      list
    })

  },

  devCallBack: function (dev) {


  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.list();
    wx.stopPullDownRefresh({
      complete: (res) => {},
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  del: function (e) {

  },
  add: function () {

  }
})