const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    device: {
      id: 0,
      kw:88.8,
      a:2,
      v:220
    },
    index: 0,
  },

 
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var id = options.id;
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
   
    wx.stopPullDownRefresh({
      complete: (res) => {},
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  btn: function () {
    wx.navigateTo({
      url: '../../statis/statis?id=' + this.data.device.id,
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  tongji: function (e) {
    wx.navigateTo({
      url: '../../statis/statis?id=' + this.data.device.id + "&name=" + this.data.device.name + "&max=" + this.data.device.activeTime,
    })
  },
  jg: function (e) {
    wx.navigateTo({
      url: '../../logs/logs?id=' + this.data.device.id + "&name=" + this.data.device.name + "&max=" + this.data.device.logTime,
    })
  },
  share: function () {
    wx.navigateTo({
      url: '../../share/share?id=' + this.data.device.id + "&name=" + this.data.device.name + "&type=devcie",
    })
  },
  edt: function () {
    wx.navigateTo({
      url: '../edit/edit?id=' + this.data.device.id + "&name=" + this.data.device.name + "&did=" + this.data.device.did,
    })
  },
  msgset: function () {
    wx.navigateTo({
      url: '../megset/msgset?id=' + this.data.device.id + "&name=" + this.data.device.name + "&wx=" + this.data.device.wx + "&sms=" + this.data.device.sms,
    })
  },
  opt: function (e) {
    
  },
  locked: function (e) {   
  }
})