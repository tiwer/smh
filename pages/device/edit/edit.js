var smh=require("../../../utils/smh.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    device: {},
    areas: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var  id=options.id;
    this.get(id)
    this.list()
  },
  get:function(id){
    var that=this;
    smh.get("device?id="+id,{
      success: function (res) {
        that.setData({       
          device: res.data
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }   
    })
  },
  list: function () {
    var gate = wx.getStorageSync('gate')
    var that = this;
    smh.get('area/list?mac=' + gate.mac + "&pageStart=0" + "&pageSize=2000", {
      success: function (res) {
        that.setData({       
          areas: res.data
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  InputChange:function(e){
    var dev = this.data.device;
    dev.DisplayName=e.detail.value;
    this.setData({
        device:dev
    })
  },
  bindAccountChange: function (e) {
    var index = e.detail.value;
    var dev = this.data.device;
    dev.areaKey = this.data.areas[index].areaKey;
    dev.areaName=this.data.areas[index].displayName

    this.setData({
      index:index,
      device:dev
    })
  },
  index: function () {
    var dev = this.data.device;
    if (dev.index > 0) {
      dev.index = 0;
    } else {
      dev.index = 1;
    }
    this.setData({
      device: dev
    })
  },
  submitForm: function (e) {
    smh.put('device?id=' +this.data.device.id+"&name="+this.data.device.displayName+"&index="+this.data.device.index+"&areakey="+this.data.device.areaKey,null, {
      success: function (res) {
        wx.navigateBack({
          complete: (res) => {},
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  }
})