var smh = require("../../../utils/smh.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    on: ["关", "开"],
    mode: ["模式", "制冷", "制热", "抽湿", "送风", "自动"],
    wind: ["风速", "小风", "中风", "大风"],
    device:{
      set:30,
      tem:28,
      on:true,
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
  
  },
  getdata: function () {

  },
  open: function () {
    var that = this;
    var dev = that.data.device;
    dev.on = ~dev.on
    var param = this.getdata()
    that.setData({
      device: dev
    })
  },
  setwind: function (e) {
    var wind = e.currentTarget.dataset.mode;
    var that = this
    var dev = this.data.device
    dev.on = true
    dev.wind = that.data.wind[wind]
    that.setData({
      device: dev
    })
  },
  wenduadd: function (e) {
    var tem = parseInt(e.currentTarget.dataset.mode);
    var that = this
    var dev = this.data.device

    var w = parseInt(dev.set) + tem;//
    if (w < 16) w = 16;//
    if (w > 30) w = 30;//
    dev.set = w
    dev.on = true
    that.setData({
      device: dev
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var id = this.data.device.id

    this.get(id);
    wx.stopPullDownRefresh({
      complete: (res) => { },
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})