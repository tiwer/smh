const app = getApp();
var smh = require('../../../utils/smh')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    device: {
      id: 0
    },
    index: 0,
  },

  get: function (id) {
    var that = this;
    smh.get('device?id=' + id, {
      success: function (res) {
        var dev = res.data;
        dev.smh.kw = (dev.smh.kw / 1000).toFixed(2)
        dev.smh.a = (dev.smh.a / 100).toFixed(2)
        dev.smh.v = (dev.smh.v / 100).toFixed(2)
        that.setData({
          device: res.data
        })
        wx.setNavigationBarTitle({
          title: res.data.name,
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    var id = options.id;
    this.get(id)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var id = this.data.device.id
    if (id && id > 0) {
      this.get(id)
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var id = this.data.device.id;
    this.get(id);
    wx.stopPullDownRefresh({
      complete: (res) => {},
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  btn: function () {
    wx.navigateTo({
      url: '../../statis/statis?id=' + this.data.device.id,
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  tongji: function (e) {
    wx.navigateTo({
      url: '../../statis/statis?id=' + this.data.device.id + "&name=" + this.data.device.name + "&max=" + this.data.device.activeTime,
    })
  },
  jg: function (e) {
    wx.navigateTo({
      url: '../../logs/logs?id=' + this.data.device.id + "&name=" + this.data.device.name + "&max=" + this.data.device.logTime,
    })
  },
  share: function () {
    wx.navigateTo({
      url: '../../share/share?id=' + this.data.device.id + "&name=" + this.data.device.name + "&type=devcie",
    })
  },
  edt: function () {
    wx.navigateTo({
      url: '../edit/edit?id=' + this.data.device.id + "&name=" + this.data.device.name + "&did=" + this.data.device.did,
    })
  },
  msgset: function () {
    wx.navigateTo({
      url: '../megset/msgset?id=' + this.data.device.id + "&name=" + this.data.device.name + "&wx=" + this.data.device.wx + "&sms=" + this.data.device.sms,
    })
  },
  opt: function (e) {
    var index = e.currentTarget.dataset.index;
    console.log(index);
    var that = this;
    var dev = that.data.device;
    var start = 0
    var data = '00'
    if (index == '1') {
      start = 0
      data = dev.smh.output1.state == 1 ? '00' : '01'
      dev.smh.output1.state = parseInt(data)
    } else {
      start = 1
      data = dev.smh.output2.state == 1 ? '00' : '01'
      dev.smh.output2.state = parseInt(data)
    }
    console.log(dev);

    smh.opt(dev.id, '0x14', start, data, {
      success: function (res) {
        that.setData({
          device: dev
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  },
  locked: function (e) {
    var index = e.currentTarget.dataset.index;
    console.log(index);
    var that = this;
    var dev = that.data.device;
    var start = 0
    var data = '00'
    if (index == '1') {
      start = 2
      data = dev.smh.output1.locked == 1 ? '00' : '01'
      dev.smh.output1.locked = parseInt(data)
    } else {
      start = 3
      data = dev.smh.output2.locked == 1 ? '00' : '01'
      dev.smh.output2.locked = parseInt(data)
    }
    console.log(dev);

    smh.opt(dev.id, '0x14', start, data, {
      success: function (res) {
        that.setData({
          device: dev
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  }
})