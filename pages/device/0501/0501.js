// miniprogram/pages/device/0803/0803.js
var smh = require('../../../utils/smh')
var util = require('../../../utils/util')
Page({
  /**
   * 页面的初始数据
   */
  data: {
    device: {
      id:0
    },
    list: [],
  },
  get: function (id) {
    var that = this;
    smh.get('device?id=' + id, {
      success: function (res) {
        var dev = res.data;
        dev.smh.data = (dev.smh.data / 1000).toFixed(2)
        that.setData({
          device: dev
        })
        wx.setNavigationBarTitle({
          title: res.data.name,
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var id = options.id;
    this.get(id)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var id = this.data.device.id
    if (id&&id > 0) {
      this.get(id)
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },


  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var id = this.data.device.id
    this.get(id)

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var dev = this.data.device.id
    this.get(id)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  morelogs: function (e) {
    var dev = this.data.device
    wx.navigateTo({
      url: "../../logs/logs?id=" + dev.id + '&name=' + dev.name + '&max=' + dev.logTime,
    })

  },
  checked: function () {
    var dev = this.data.device;
    var enable = dev.smh.enable ? '00' : '01';
    smh.opt(dev.id, '14', 2, enable, {
      success: res => {},
      fail: res => {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  },
  checkedvoice: function () {
    var dev = this.data.device;
    var enable = dev.smh.voice ? '00' : '01';
    smh.opt(dev.id, '14', 1, enable, {
      success: res => {},
      fail: res => {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  },
})