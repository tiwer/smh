// miniprogram/pages/my/my.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {
      avatarUrl: "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKO2zxcxBSz47WxicNxRAe7NoXJLjdv6LyZgenDtLYibXq6IzlgFMV56iaVdict0ichF6rUXDp1qyticF6A/132",
      nickName: "汉莫智控"
    },
    ver: ""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var user = wx.getStorageSync('wxUser');
    console.log(user);
    if (user != "") {
      this.setData({
        userInfo: user
      });
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {},
  area: function (e) {
    wx.navigateTo({
      url: '../area/manage/manage',
    })
  },
  device: function (e) {
    wx.navigateTo({
      url: '../device/manage/manage',
    })
  },
  scene: function (e) {
    wx.navigateTo({
      url: '../scene/manage/manage',
    })
  },
  gate: function () {
    wx.navigateTo({
      url: '../gate/manage/manage',
    })
  },
  user: function () {
    wx.navigateTo({
      url: '../user/user',
    })
  },
  nearby: function () {
    wx.navigateTo({
      url: '../nearby/nearby',
    })
  },
  clock: function () {
    wx.navigateTo({
      url: '../clock/clock',
    })
  },
  tuisong: function () {
    wx.navigateTo({
      url: '../msgset/msgset',
    })
  },
  saoyisao: function (e) {
    wx.scanCode({
      success(res) {   
        var url = res.result;
        wx.showToast({
          title: url,
        })       
      }
    })
  },
  logout: function () {
    wx.showModal({
      title: "退出小程序",
      success: function (res) {
        if (res.cancel) {
          //点击取消,默认隐藏弹框
        } else {
          wx.setStorageSync('token', "")
          wx.setStorageSync('wxUser', "")
          wx.navigateTo({
            url: '../login/login',
          })
        }
      },
    })
  }
})