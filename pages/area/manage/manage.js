
Page({

  /**
   * 页面的初始数据
   */
  data: {
    pageSize: 20,
    list: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   // this.list()
  },
  //获取区域
  list: function () {
    var list = [{
        id: 1,
        name: "会客区",
        img:'icon-keting.png',   
        //正在打开的灯数量         
        switchCount: 9,
        //正在开启的空调数量
        airCount: 0,
        //正在打开的窗帘数量
        windowCount: 5,                              
    }, {
        id: 2,
        img:"icon-dianying.png",
        name: "影院区",          
        switchCount: 2,
        airCount: 1,
        windowCount: 2,                 
    }]

    this.setData({
         list
    })
},
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.list()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.list();
    wx.stopPullDownRefresh({
      complete: (res) => {},
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.list()
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },
  areaClick:function(e){
    var id = e.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../area?id='+id,
    })
  },
  edit: function (e) {
    var id = e.currentTarget.dataset.id;

    wx.navigateTo({
      url: '../edit/edit?id=' + id,
    })
  },
  share: function (e) {
    var id = e.currentTarget.dataset.id;
    var name = e.currentTarget.dataset.name;
    wx.navigateTo({
      url: '../../share/share?id=' + id + "&name=" + name+"&type=area",
    })
  },
  del: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var area = that.data.list[index]
    wx.showModal({
      title: "删除",
      success: function (res) {
        if (res.cancel) {
          //点击取消,默认隐藏弹框
        } else {
          that.data.list.splice(index, 1)
          var list = that.data.list;
          that.setData({
            list: list
          })        
        }
      },
    })
  }
})