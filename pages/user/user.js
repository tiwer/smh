// miniprogram/pages/user/user.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    role: ['游客', '普通用户', '高级用户', '管理员'],
    edit: ['查看设备', '删除用户'],
    list: []
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  list: function () {
    var list = [{
      avatar: 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKO2zxcxBSz47WxicNxRAe7NoXJLjdv6LyZgenDtLYibXq6IzlgFMV56iaVdict0ichF6rUXDp1qyticF6A/132',
      name: 'cdy',
      role: 3,
      phone: '13433037435',
      enable: true,
    }]

    this.setData({
      list
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.list()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var size = this.data.PageSize + 20;
    this.setData({
      PageSize: size
    })
    this.list();
    wx.stopPullDownRefresh({
      complete: (res) => { },
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var size = this.data.PageSize + 20;
    this.setData({
      PageSize: size
    })
    this.list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  switchChange: function (e) {
    var that = this;
    var enable = false;
    var check = e.detail.value;
    if (check) {
      enable = true
    }
    var index = e.target.dataset.index;
    var ug = this.data.list[index];
    var setdata = "list[" + index + "].enable";
    that.setData({
      setdata: enable
    })
  },
  edit: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var ud = this.data.list[index];
    wx.showActionSheet({
      itemList: that.data.edit,
      success(res) {
        console.log(res);
        if (res.tapIndex == 0) {
          wx.navigateTo({
            url: './edit/edit?id=' + ud.id,
          })
        }
      
        if (res.tapIndex == 1) {
          that.del(index);
        }
      },
    })
  },
  del: function (index) {
    var that = this;
    wx.showModal({
      title: "删除",
      success: function (res) {
        if (res.cancel) {
          //点击取消,默认隐藏弹框
        } else {
          that.data.list.splice(index, 1)
          var list = that.data.list;
          that.setData({
            list: list
          })
        }
      },
    })
  },
})