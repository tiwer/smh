// miniprogram/pages/user/edit/edit.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    role: ['游客', '普通用户', '高级用户','管理员'],
    list: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  this.list();
  },
  switchChange: function (e) {
    var enable = false;
    var check = e.detail.value;
    if (check) {
      enable = true
    }
    var index = e.target.dataset.index;
    var setdata = "list[" + index + "].enable";
    this.setData({
      setdata: enable
    })
  },
  list: function () {
   var list=[{
     name:'开关',
     type:'0101',
     role:'3',
     expireTime:'2020/8/12',
     enable:true
   },{
    name:'调光灯',
    type:'0102',
    role:'3',
    expireTime:'2020/8/12',
    enable:true
  },{
    name:'窗帘',
    type:'0201',
    role:'3',
    expireTime:'2020/8/12',
    enable:true
  },{
    name:'开关',
    type:'0101',
    role:'3',
    expireTime:'2020/8/12',
    enable:true
  }]

  this.setData({
    list
  })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var size = this.data.PageSize + 20;
    this.setData({
      PageSize: size
    })
    this.list();
    wx.stopPullDownRefresh({
      complete: (res) => { },
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    var size = this.data.PageSize + 20;
    this.setData({
      PageSize: size
    })
    this.list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  bindChange: function (e) {

    var index = e.detail.value;
    var role = this.data.edit[index];
    console.log(role)
  },
  del: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    wx.showModal({
      title: "删除",
      success: function (res) {
        if (res.cancel) {
          //点击取消,默认隐藏弹框
        } else {
          that.data.list.splice(index, 1)
          var list = that.data.list;
          that.setData({
            list: list
          })
        }
      },
    })
  }
})