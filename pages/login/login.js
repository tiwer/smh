"use strict";
var app = getApp();
Page({
    data: {
        motto: 'Hello pyn',
        authorize: '授权登录',
        userInfo: {
            avatarUrl: "https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTKO2zxcxBSz47WxicNxRAe7NoXJLjdv6LyZgenDtLYibXq6IzlgFMV56iaVdict0ichF6rUXDp1qyticF6A/132",
            nickName: "cdy"
        },
        hasUserInfo: false,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
        phone: "",
        openid: "",
        unionid: "",
        code: "",
        getCode: "获取验证码",
        times: 60,
        disabled: true
    },
    onLoad: function () {},

    quxiao: function () {

    },
    getUserInfo: function (e) {
        var user = e.detail.userInfo;
        wx.getUserInfo({
            complete: (res) => {},
        })
        console.log(user);
        if (user == null) {
            wx.navigateTo({
                url: '../unlogin/unlogin',
            })
            return;
        }
        app.globalData.userInfo = e.detail.userInfo;
        this.setData({
            userInfo: e.detail.userInfo,
            hasUserInfo: true,
        });
        wx.setStorageSync('wxUser', e.detail.userInfo)
    },
    formInputChange: function (e) {
        this.setData({
            phone: e.detail.value
        })
    },
    //获取手机验证码
    getPhoneVcCode: function (e) {
        var that = this
        if (!(/^1[3456789]\d{9}$/.test(that.data.phone))) {
            wx.showToast({
                title: '请输入正确的手机号',
            })
            return
        }
        if (!this.data.disabled) {
            return;
        }
        var times = this.data.times
        that.setData({
            disabled: false,
            getCode: "已发送" + times + "s",
        })
        var i = setInterval(function () {
            times--
            that.setData({
                disabled: false,
                getCode: "已发送" + times + "s",
            })
            if (times <= 0) {
                clearInterval(i)
                that.setData({
                    getCode: "获取验证码",
                    disabled: true
                })
            }
        }, 1000)

    },
    getVcCode: function (e) {
        this.setData({
            code: e.detail.value
        })
    },
    submitForm: function (e) {
        var phone = this.data.phone
        var code = this.data.code
        if (phone == "" || code == "") {
            wx.showToast({
                title: '请输入正确信息',
            })
            return
        }
        var token = "token";
        wx.setStorageSync('token', token);
        wx.switchTab({
            url: '../index/index',
        })
    }
});