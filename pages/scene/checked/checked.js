// miniprogram/pages/scene/action/action.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    kg: ["打开", "关闭", ""],
    clockId: 0,
    list: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setNavigationBarTitle({
      title: '条件设置',
    })
    this.setData({
      clockId: options.id
    })

    this.list();
  },

  list: function () {
    var gate = wx.getStorageSync('gate')
    if (gate == '') {
      if (gate == '') {
        wx.showModal({
          title: "请选择场所",
        })
      }
      return;
    };
    var that = this;
    smh.get('clock/GetActionChecked?id=' + that.data.clockId, {
      success: function (res) {
        for (let index = 0; index < res.data.length; index++) {
          var element = res.data[index];
          if (element.type == 2 || element.type == 3 || element.type == 4) {
            element.checked = ["发生警报", "警报取消"]
            element.log = true
          } else {
            element.checked = ["打开", "关闭"]
          }
        }
        that.setData({
          list: res.data
        })
       
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  },

  devCallBack: function (dev) {
    var that = this;
    var checked = {
      name: dev.name,
      clockID: parseInt(that.data.clockId),
      deviceID: dev.id,
      enable: 1,
      type: dev.type,
      start: 0,
      code: '14',
      action: '01',
    }
    if (checked.type == 2 || checked.type == 3 || checked.type == 4) {
      checked.checked = ["发生警报", "警报取消"]
      checked.log = true
    } else {
      checked.checked = ["打开", "关闭"]
    }
    smh.post('clock/SetActionChecked', checked, {
      success: function (res) {
        checked.id = res.data
        var list = that.data.list;
        list.push(checked);
        that.setData({
          list: list
        })
        var pages = getCurrentPages(); //当前页面
        var prevPage = pages[pages.length - 2]; //上一页面
         var clock=prevPage.data.clock;
         clock.actionCheckedCount=list.length
        prevPage.setData({
          clock
        })
         
        wx.showToast({
          title: '设置成功',
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.list();
    wx.stopPullDownRefresh({
      complete: (res) => {},
    })
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    this.list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  kgbindChange: function (e) {
    var index = e.detail.value;
    var aindex = e.currentTarget.dataset.index;
    var checked = this.data.list[aindex];
    var key = checked.checked[index];

    var that = this;
    switch (key) {
      case "打开":
        checked.action = "01"
        break;
      case "关闭":
        checked.action = "00"
        break;
      case "发生警报":
        checked.action = "01"
        break;
      case "警报取消":
        checked.action = "00"
        break;
      default:
        break;
    }
    console.log(key)
    console.log(checked)
    smh.post('clock/SetActionChecked', checked, {
      success: function (res) {
        var setdata = "list[" + aindex + "]";
        that.setData({
          [setdata]: checked
        })
      },
      fail: function (res) {
        wx.showToast({
          title: res.msg,
          icon: 'loading'
        })
      }
    })
  },
  del: function (e) {
    var that = this;
    var index = e.currentTarget.dataset.index;
    var checked = this.data.list[index];
    wx.showModal({
      title: "删除",
      success: function (res) {
        if (res.cancel) {
          //点击取消,默认隐藏弹框
        } else {
          smh.del('clock/DeleteActionChecked?checkId=' + checked.id, {
            success: function (res) {
              that.data.list.splice(index, 1)
              var list = that.data.list;
              that.setData({
                list: list
              })
            },
            fail: function (res) {
              wx.showToast({
                title: res.msg,
                icon: 'loading'
              })
            }
          })

        }
      },
    })
  },
  add: function () {
    wx.navigateTo({
      url: '../../device/manage/manage?action=true',
    })
  }
})