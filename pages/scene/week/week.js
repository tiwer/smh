// miniprogram/pages/scene/week/week.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    clock: {
      Week0:false,
      Week1:false,
      Week2:false,
      Week3:false,
      Week4:false,
      Week5:false,
      Week6:false,
      Loop:false,
    },
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  var clock=JSON.parse(options.clock);
  if(clock!=null){
    console.log(clock.Week0);
    this.setData({
      clock:clock
    })   
  }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  choose: function (e) {
    var index = e.target.dataset.index;
    var clock = this.data.clock;

    switch (index) {
      case '0':
        console.log(clock)
        clock.Week0 = !clock.Week0
        break;
      case '1':
        clock.Week1 = !clock.Week1
        break;
      case '2':
        clock.Week2 = !clock.Week2
        break;
      case '3':
        clock.Week3 = !clock.Week3 
        break;
      case '4':
        clock.Week4 = !clock.Week4
        break;
      case '5':
        clock.Week5 = !clock.Week5  
        break;
      case '6':
        clock.Week6 = !clock.Week6  
        break;
      case '7':
        clock.Loop = !clock.Loop   
        break;
    }
   
    this.setData({
      clock:clock
    })

  },
  submitForm:function(){
    var pages = getCurrentPages(); //当前页面
    var prevPage = pages[pages.length - 2]; //上一页面
    
    var clock=this.data.clock;
    prevPage.setData({
      clock: clock
    })
    wx.navigateBack()
  }
})