// pages/area/manage/manage.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

    choose: 0,
    list: [

    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    var gate = wx.getStorageSync('gate')
    if (gate != "") this.setData({
      choose: gate.id
    })
    // this.list();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.setData({
      pageIndex: 0
    })
    this.list();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

    this.list();
    wx.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.log(1)
    this.list();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  list: function () {
    var that = this;
    var list = [{
      id: 1,
      name: "红树林别墅",
      img: "icon-return-home.png"
    }, {
      id: 2,
      name: "南山必胜客公司",
      img: "gengduo-g.png"
    }]
    that.setData({
      list
    })
  },
  add: function () {
    wx.navigateTo({
      url: '../edit/edit?id=0',
    })
  },
  del: function (index) {
    var gate = that.data.list[index];
    wx.showModal({
      title: "删除",
      success: function (res) {
        if (res.cancel) {
          //点击取消,默认隐藏弹框
        } else {
          that.data.list.splice(index, 1)
          var list = that.data.list;
          that.setData({
            list: list
          })
        }
      },
    })
  },
  share: function (index) {
    var that = this;  
    var gate = that.data.list[index];
    console.log(gate)
    wx.navigateTo({
      url: '../../share/share?id=' + gate.id + "&name=" + gate.name + "&type=gate",
    })
  },
  areaClick: function (e) {
    var that = this
    var gate = that.data.list[e.currentTarget.dataset.index];
    wx.showActionSheet({
      itemList: ["切换", "分享", "编辑", "删除"],
      success: function (res) {
        var index = res.tapIndex;
        switch (index) {
          case 0:
            wx.setStorageSync('gate', gate)
            that.setData({
              choose: gate.id
            })
            break;
          case 1:
           that.share(index)
            break;
          case 2:
            wx.navigateTo({
              url: '../edit/edit?id=' + gate.id,
            })
            break;
          case 3:
            that.del(index)
            break;

        }
      }
    })
  }
})