
Page({

  /**
   * 页面的初始数据
   */
  data: {
    gate: {
      id:1,
      name:'红树林别墅',
      img:'icon-return-home.png'
    },
    iconfontlist: [
      "icon-webiconwoshi",
      "icon-woshi",
      "icon-kongjian09",
      "icon-shufang",
      "icon-chashi",
      "icon-keting",
      "icon-chufang",
      "icon-canting",
      "icon-batai",
      "icon-dianying",
      "icon-jianshen",
      "icon-yule",
      "icon-bangongshi",
      "icon-yangtai",
      "icon-damen",
      "icon-hekriconqingjingzoulang"
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
  },
  changeIcon: function (e) {
    var index = e.currentTarget.dataset.index;
    var gate = this.data.gate;
    gate.img = this.data.iconfontlist[index];
    this.setData({
      gate: gate
    })
  },
  InputChange: function (e) {
    var gate = this.data.gate;
    gate.name = e.detail.value;
    this.setData({
      gate: gate
    })
  },
  Index: function (e) {
    var gate = this.data.gate;
    if (gate.index > 0) {
      gate.index = 0
    } else {
      gate.index = 1
    }

    this.setData({
      gate: gate
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShgateppMessage: function () {

  },

  submitForm:function(){
    wx.navigateBack({
      complete: (res) => {},
    })
  }
})